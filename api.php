
<?php //début de code php

include 'calcul.php';
include 'meteo.php';

    $dataset = file_get_contents("dataset.json");

/*
    $jsonIterator = new RecursiveIteratorIterator(
    new RecursiveArrayIterator(json_decode($dataset, TRUE)),
    RecursiveIteratorIterator::SELF_FIRST);

    foreach ($jsonIterator as $key => $val) {
        if(is_array($val)) {
            //echo "$key:\n";
        } else {
            echo "$key => $val\n";
            if("$key" == "__metadata") {
                echo "$key -> $val" ;
            }
        }
    }
*/

if( !empty($_GET) && isset($_GET["currentLong"]) && isset($_GET["currentLat"]) && isset ($_GET["perimetre"]))
{
    $lattitude = $_GET["currentLat"];
    $longitude = $_GET["currentLong"];
    $perimetre = $_GET["perimetre"];
} else {
    echo "error";
}

if( !empty($_GET) && isset($_GET["meteo"]) ) {
    
    echo meteo_local($lattitude, $longitude);
    
} else {

    $rayon = calcul_rayon($perimetre);

    $delta_long = calcul_delta_long($lattitude,$longitude,$rayon);
    $delta_lat = calcul_delta_lat($lattitude,$longitude,$rayon);

    echo "calcul";
}
?>
	
