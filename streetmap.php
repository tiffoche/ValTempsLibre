<?php

	$req = $_GET['adresse'];

	$nominatingURL = 'http://nominatim.openstreetmap.org/search?format=json&q='.urlencode($req);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $nominatingURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, '');
	$resultat = curl_exec ($ch);
	curl_close($ch);

	$json = json_decode($resultat);

	$lat = $json[0]->lat;
	$long = $json[0]->lon;

	echo "lat=$lat&long=$long";

?>
